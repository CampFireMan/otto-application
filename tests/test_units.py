import unittest

from jsonschema.exceptions import ValidationError
from main import validate_retoure_structure


class TestUnits(unittest.TestCase):
    def test_validate_retoure_structure(self):
        invalid_retoure = {
            'order_id': 'Invalid',
        }
        self.assertRaises(ValidationError,
                          lambda: validate_retoure_structure(invalid_retoure))
