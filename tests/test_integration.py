import base64
import json
import unittest

from main import process_return


class TestIntegration(unittest.TestCase):
    def test_message_recieved(self):
        retoure = {'article_id': 1234, 'order_id': 1234}
        data = base64.encodebytes(json.dumps(retoure).encode('utf-8'))
        event = {
            'data': data,
        }
        context = {}
        process_return(event, context)
