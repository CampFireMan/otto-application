# Teamtag-Aufgabe "Migration der Retourenverarbeitung"

## Test deployed function

Publish a JSON to the queue:

```[bash]
gcloud pubsub topics publish returns_queue --message '{"article_id": 1234, "order_id": "joe"}'
```
