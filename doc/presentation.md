---
marp: true
---

# Teamtag-Aufgabe "Migration der Retourenverarbeitung"

---

## Legacy system

![old](architecture-before.png)

---

## Uebergangsphase

![new](architecture-after.png)

---

## Algorithmus Anforderungen

- Hohes Maß an Fehlertoleranz
  - Robuste Fehlerbehandlung
  - Umfassende Tests: Integration & Units
- Erweiterbarkeit
  - Hinzufügen von mehreren Persistenzsystemen (Buckets, DBS)
  - Erweiterung von Validierung etc.

---

## Algorithmus

![algorithm](architecture-algorithm.png)

---

## Nachforschungen

![investigations](investigations.png)


---

## Schemata

![schemas](architecture-return-structure.png)

---

## Code

[https://gitlab.com/CampFireMan/otto-application](https://gitlab.com/CampFireMan/otto-application)
