provider "google" {
  project = var.project
  region  = var.region
}
module "my_function" {
  source               = "./modules/function"
  project              = var.project
  function_name        = "process-return"
  function_entry_point = "process_return"
}
