variable "project" {
  default = "bachelor-thesis-332216"
}
variable "region" {
  default = "europe-west1" # Choose a region
}

variable "pub_sub_name" {
  default = "returns_queue"
}
