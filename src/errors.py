
class MessageQueueError(Exception):
    pass


class ValidationError(Exception):
    pass
