from src.errors import ValidationError


def validate_order(retoure: dict):
    if retoure['order_id'] < 100:
        raise ValidationError('Order does not exist!')
