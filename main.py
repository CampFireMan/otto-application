import base64
import json
import logging
from inspect import getmembers, isfunction
from typing import List

import google.cloud.functions
import google.cloud.logging
from jsonschema import validate

from src import errors, schemas, validators

# Instantiates a client
client = google.cloud.logging.Client()

# Retrieves a Cloud Logging handler based on the environment
# you're running in and integrates the handler with the
# Python logging module. By default this captures all logs
# at INFO level and higher
client.setup_logging()


def decode_event(event: dict):
    return json.loads(base64.b64decode(event['data']).decode('utf-8'))


def validate_retoure_structure(retoure: dict) -> None:
    validate(retoure, schemas.retoure)


def validate_retoure_content(retoure: dict) -> List[Exception]:
    exceptions = []
    for func in getmembers(validators, isfunction):
        if func[0].startswith('validate_'):
            try:
                func[1](retoure)
            except errors.ValidationError as e:
                exceptions.append(e)
    return exceptions


def persist_retoure(retoure: dict) -> None:
    logging.info('Persisted retoure!')


def publish_retoure_credit(retoure: dict) -> None:
    pass


def publish_retoure_debit(retoure: dict) -> None:
    pass


def process_return(event: dict, context: 'google.cloud.functions.Context') -> None:
    """
    Validates and persists returns. 
    Recievables are published to the creditor and debitor systems.

    Args:
        event (dict): Dictionary containing the data of the PubSub message
        context (google.cloud.functions.Context): Metadata of triggering event
    """
    if 'data' not in event:
        raise ValueError('Event has no data property')

    retoure = decode_event(event)
    logging.info('Decoded retoure data!')

    validate_retoure_structure(retoure)
    logging.info('Validated retoure structure!')

    exceptions = validate_retoure_content(retoure)
    if exceptions:
        logging.error(exceptions)
        raise Exception('Content is invalid!')
    logging.info('Validated retoure content!')

    persist_retoure(retoure)
    logging.info('Persisted retoure!')

    publish_retoure_credit(retoure)
    logging.info('Published credit message!')

    publish_retoure_debit(retoure)
    logging.info('Published debit message!')
